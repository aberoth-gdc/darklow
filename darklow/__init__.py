"""
All modules for running a Darklow Discord bot.
"""

import logging
import os

import discord
from discord.errors import Forbidden

from .util import preprocess_input, get_discord_id
from .bot import parse_command

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

intents: discord.Intents = discord.Intents.default()
intents.message_content = True

client: discord.Client = discord.Client(intents=intents)


@client.event
async def on_ready():
    """
    TODO
    """
    logger.info("===== DARKLOW BEGIN =====")

    logger.info("DARKLOW IS READY")
    logger.info(f"DARKLOW USER ID: {client.user.id}")

    return


@client.event
async def on_message(message):
    """
    TODO
    """
    logger.info(
        f"{message.guild}, #{message.channel}, {message.guild}: {message.content}"
    )
    logger.debug(message.guild.emojis)

    # Don't bother with Tavelor's messages
    if message.author == client.user:
        return

    if len(message.content) == 0:
        return

    # handle when we're not mentioned
    if get_discord_id(message.content.split()[0]) != client.user.id:
        return

    cmd = parse_command(message)
    msg: str = await cmd(message)

    if msg != "":
        logger.debug(msg)
        # Try to do a fancy reply, otherwise just send to channel.
        try:
            await message.reply(msg)
        except Forbidden:
            await message.channel.send(msg)
