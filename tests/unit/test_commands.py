import darklow.bot as b


class TestRegex:
    def test_trivia(self):
        pass

    def test_no_match(self):
        pass

    def test_hello(self):
        pass

    def test_simple_flip(self):
        m = b.re_simple_flip.match("flip a b 2")
        assert m is not None
        assert m.group("count") == "2"
        assert m.group("participants").split() == ["a", "b"]
        m = b.re_simple_flip.match("flip a b")
        assert m is not None
        assert m.group("count") is None
        assert m.group("participants").split() == ["a", "b"]

        assert b.re_simple_flip.match("flip") is not None
        assert b.re_simple_flip.match("flipcoin") is not None
        assert b.re_simple_flip.match("flip coin") is not None
        # TODO: How to do this
        # assert b.re_simple_flip.match("flip coin 2") is None
        # assert b.re_simple_flip.match("flipcoin 2") is None
