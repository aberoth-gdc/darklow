FROM python:3.9

MAINTAINER Aberoth GDC <aberoth.gdc@gmail.com>

RUN pip install pipenv

COPY Pipfile .
COPY Pipfile.lock .

RUN pipenv install --deploy --system

COPY . .

ENTRYPOINT ["python", "main.py"]
